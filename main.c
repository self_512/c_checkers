﻿#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef unsigned char State;
static const State empty = 0x0;
static const State white = 0x1;
static const State black = 0x2;
static const State queen = 0x4;

typedef struct
{
  int x;
  int y;
} Pos;

typedef struct
{
  State **f;
  int width;
  int height;
  State nowMoves;
} Field;

typedef struct
{
  Pos *from;
  Pos *to;
  int *scores;
  Pos **eaten;
  int size;
} PossibleMoves;

PossibleMoves init_pm(int size)
{
  PossibleMoves pm;
  pm.from = malloc(sizeof(Pos)*size);
  pm.to = malloc(sizeof(Pos)*size);
  pm.scores = malloc(sizeof(int)*size);
  pm.eaten = malloc(sizeof(Pos*)*size);
  for (int i = 0; i < size; i++)
  {
    pm.eaten[i] = calloc(32, sizeof(Pos));
  }
  pm.size = size;
  return pm;
}

void destroy_pm(PossibleMoves pm)
{
  free(pm.from);
  free(pm.to);
  free(pm.scores);
  for (int i = 0; i < pm.size; i++)
  {
    free(pm.eaten[i]);
  }
  free(pm.eaten);
}

Field init_field(int width, int height)
{
  State **field = malloc(sizeof(State*)*width);
  State *memBlock = malloc(sizeof(State)*height*width);
  for (int i = 0; i < width; i++)
  {
    field[i] = memBlock + (i*height);
    for (int j = 0; j < height; j++)
      field[i][j] = empty;
  }
  for (int i = 0; i < width; i++)
  {
    for (int j = i % 2; j < 3; j += 2)
    {
      field[i][j] = white;
    }
    if (height % 2)
    {
      for (int j = i % 2; j < 3; j += 2)
      {
        field[i][height - 1 - j] = black;
      }
    }
    else
    {
      for (int j = (i + 1) % 2; j < 3; j += 2)
      {
        field[i][height - 1 - j] = black;
      }
    }
  }

  Field fret = { .f = field,.width = width,.height = height,.nowMoves = white };
  return fret;
}

Field copy(Field from)
{
  Field new_one = init_field(from.width, from.height);
  new_one.nowMoves = from.nowMoves;
  memcpy(new_one.f[0], from.f[0], from.height * from.width * sizeof(State));
  return new_one;
}

void print_field(Field field)
{
  printf(" ");
  for (int i = 0; i < field.width; i++)
  {
    printf("%i", i + 1);
  }
  printf("\n");
  for (int j = field.height - 1; j >= 0; j--)
  {
    printf("%i", j + 1);
    for (int i = 0; i < field.width; i++)
    {
      switch (field.f[i][j])
      {
      //white
      case 1:
        printf("w");
        break;
      //black
      case 2:
        printf("b");
        break;
      //white queen
      case 1|4:
        printf("W");
        break;
      //black queen
      case 2 | 4:
        printf("B");
        break;
      case 0:
        printf("Ы");
        break;
      default:
        break;
      }
    }
    printf("\n");
  }
  printf(" ");
  for (int i = 0; i < field.width; i++)
  {
    printf("%i", i + 1);
  }
  printf("\n");

}

int valid_bounds(int height, int width, Pos pos)
{
  if (pos.x < 0 || pos.y < 0 || pos.x >= width || pos.y >= height)
    return 0;
  return 1;
}

int can_move_queen(Field *field, Pos from, Pos to) {
  State colorFrom = field->f[from.x][from.y] & 3;
  State colorTo = field->f[to.x][to.y];
  State oppositeColor = colorFrom & white ? black : white;
  int weight = 1;

  // can move?
  if (abs(from.x - to.x) != abs(from.y - to.y))
    return 0;

  // eats smth?
  int dx = from.x;
  int dy = from.y;
  int prev_ate = 0;
  while (dx != to.x && dy != to.y)
  {
    (from.x < to.x) ? dx++ : dx--;
    (from.y < to.y) ? dy++ : dy--;
    if (field->f[dx][dy] & colorFrom)
    {
      return 0;
    }
    if (field->f[dx][dy] & oppositeColor)
    {
      if (prev_ate)
      {
        return 0;
      }
      weight++;
      prev_ate = 1;
    }
    else
    {
      prev_ate = 0;
    }
  }
  return weight;
}

int can_move(Field *field, Pos from, Pos to) {
  if (!valid_bounds(field->height, field->width, from))
    return 0;
  if (!valid_bounds(field->height, field->width, to))
    return 0;
  int weight = 1;
  State colorFrom = field->f[from.x][from.y] & 3;
  State colorTo = field->f[to.x][to.y];
  State oppositeColor = colorFrom & white ? black : white;
  if (colorTo != empty || colorFrom == empty || field->nowMoves != colorFrom)
    return 0;
  if (field->f[from.x][from.y] & queen)
  {
    return can_move_queen(field, from, to);
  }
  //eats
  if (abs(from.x - to.x) == 2 && abs(from.y - to.y) == 2) {
    int midx = (from.x + to.x) / 2;
    int midy = (from.y + to.y) / 2;
    State colorBetween = field->f[midx][midy] & 3;
    if (colorBetween != oppositeColor)
      return 0;
    
    weight++;
  }
  //moves
  else if (abs(from.x - to.x) != 1 || abs(from.y - to.y) != 1)
    return 0;
  else {
    if ((colorFrom & white && from.y > to.y)
      || (colorFrom & black && from.y < to.y))
      return 0;
  }
  return weight;
}

void move(Field *field, Pos from, Pos to, Pos *eaten)
{
  int eatenIdx = 0;
  while (eaten[eatenIdx].x && eaten[eatenIdx].y)
  {
    field->f[eaten[eatenIdx].x][eaten[eatenIdx].y] = empty;
    eatenIdx++;
  }
  State colorFrom = field->f[from.x][from.y];
  State oppositeColor = colorFrom & white ? black : white;
  
  field->f[to.x][to.y] = colorFrom;

  if ((colorFrom & black && to.y == 0)
    ||(colorFrom & white && to.y == field->height - 1))
  {
    field->f[to.x][to.y] |= queen;
  }

  field->f[from.x][from.y] = empty;
  field->nowMoves = oppositeColor;
}

void destroy_field(Field field)
{
  free(field.f[0]);
  free(field.f);
}

int get_queen_moves(Field field, int i, int j, Pos **moves)
{
  int size = 0;
  *moves = malloc(sizeof(Pos)*32);
  int ti = i - 1;
  int tj = j - 1;
  while (ti >= 0 && tj >= 0)
  {
    Pos pos = { .x = ti,.y = tj };
    (*moves)[size] = pos;
    size++;
    ti--;
    tj--;
  }
  ti = i - 1;
  tj = j + 1;
  while (ti >= 0 && tj < field.height)
  {
    Pos pos = { .x = ti,.y = tj };
    (*moves)[size] = pos;
    size++;
    ti--;
    tj++;
  }
  ti = i + 1;
  tj = j + 1;
  while (ti < field.width && tj < field.height)
  {
    Pos pos = { .x = ti,.y = tj };
    (*moves)[size] = pos;
    size++;
    ti++;
    tj++;
  }
  ti = i + 1;
  tj = j - 1;
  while (ti < field.width && tj >= 0)
  {
    Pos pos = { .x = ti,.y = tj };
    (*moves)[size] = pos;
    size++;
    ti++;
    tj--;
  }
  return size;
}

int get_possible_moves(Field field, PossibleMoves *pm)
{
  int size = 0;
  for (int i = 0; i < field.width; i++)
  {
    for (int j = i % 2; j < field.height; j += 2)
    {
      if (!(field.f[i][j] & field.nowMoves))
        continue;

      Pos moves_simple[6] = {
          {.x = i - 2,.y = j - 2},
          {.x = i + 2,.y = j - 2 },
          {.x = i - 2,.y = j + 2 },
          {.x = i + 2,.y = j + 2 },
      };
      if (field.nowMoves & white)
      {
        Pos upleft = { .x = i - 1,.y = j + 1 };
        moves_simple[4] = upleft;
        Pos upright = { .x = i + 1,.y = j + 1 };
        moves_simple[5] = upright;
      }
      else
      {
        Pos downleft = { .x = i - 1,.y = j - 1 };
        moves_simple[4] = downleft;
        Pos downright = { .x = i + 1,.y = j - 1 };
        moves_simple[5] = downright;
      }
      int need_to_free_moves = 0;
      Pos *moves = moves_simple;
      int moves_size = 6;
      if (field.f[i][j] & queen)
      {
        need_to_free_moves = 1;
        moves_size = get_queen_moves(field, i, j, &moves);
      }
      Pos from = { .x = i,.y = j };
      for (int pos_idx = 0; pos_idx < moves_size; pos_idx++)
      {
        int score = can_move(&field, from, moves[pos_idx]);
        if (!score)
          continue;

        pm->from[size] = from;
        pm->to[size] = moves[pos_idx];
        pm->scores[size] = score;

        if (score > 1)
        {
          // ищутся возможности повторного взятия
          Field another = copy(field);
          PossibleMoves new_pm = init_pm(512);

          int eatenIdx = 0;

          //move(&another, from, moves[pos_idx]);
          if (field.f[i][j] & queen)
          {
            State colorFrom = field.f[from.x][from.y] & 3;
            State oppositeColor = colorFrom & white ? black : white;

            int dx = from.x;
            int dy = from.y;
            while (dx != moves[pos_idx].x && dy != moves[pos_idx].y)
            {
              (from.x < moves[pos_idx].x) ? dx++ : dx--;
              (from.y < moves[pos_idx].y) ? dy++ : dy--;

              if (field.f[dx][dy] & oppositeColor)
              {
                another.f[dx][dy] = empty;
                Pos eatenPos = { .x = dx,.y = dy };
                pm->eaten[size][eatenIdx] = eatenPos;
                eatenIdx++;
              }
            }
          }
          else
          {
            int midx = (from.x + moves[pos_idx].x) / 2;
            int midy = (from.y + moves[pos_idx].y) / 2;
            another.f[midx][midy] = empty;
            Pos eatenPos = { .x = midx,.y = midy };
            pm->eaten[size][eatenIdx] = eatenPos;
            eatenIdx++;
          }

          State colorFrom = another.f[from.x][from.y];
          another.f[moves[pos_idx].x][moves[pos_idx].y] = colorFrom;
          another.f[from.x][from.y] = empty;


          int new_size = get_possible_moves(another, &new_pm);
          if (new_size)
          {
            for (int loci = 0; loci < new_size; loci++)
            {
              if (new_pm.from[loci].x != moves[pos_idx].x || new_pm.from[loci].y != moves[pos_idx].y
                || (new_pm.eaten[loci][0].x == 0 && new_pm.eaten[loci][0].y == 0)) {
                continue;
              }
              pm->to[size] = new_pm.to[loci];
              pm->scores[size] += new_pm.scores[loci];
              int newEatenIdx = 0;
              while (new_pm.eaten[loci][newEatenIdx].x && new_pm.eaten[loci][newEatenIdx].y)
              {
                pm->eaten[size][eatenIdx] = new_pm.eaten[loci][newEatenIdx];
                eatenIdx++;
                newEatenIdx++;
              }
            }
          }
          destroy_pm(new_pm);
          destroy_field(another);
        }
        size++;
        if (size == pm->size)
        {
          pm->size += 128;
          pm->from = realloc(pm->from, pm->size);
          pm->to = realloc(pm->to, pm->size);
          pm->scores = realloc(pm->scores, pm->size);
          pm->eaten = realloc(pm->eaten, pm->size);
          for (int reallocEatenIdx = pm->size - 128; reallocEatenIdx < pm->size; reallocEatenIdx++)
          {
            pm->eaten[reallocEatenIdx] = calloc(32, sizeof(Pos));
          }
        }
      }
      if (need_to_free_moves)
      {
        free(moves);
      }
    }
  }

  //убрать ходы без взятия, если есть хоть один со взятием
  int present = 0;
  for (int i = 0; i < size; i++)
  {
    if (pm->scores[i] > 1)
    {
      present++;
    }
  }
  
  if (!present || present == size)
    return size;

  PossibleMoves cpm = init_pm(present);
  int new_size = 0;
  for (int i = 0; i < size; i++)
  {
    if (pm->scores[i] > 1)
    {
      cpm.from[new_size] = pm->from[i];
      cpm.to[new_size] = pm->to[i];
      cpm.scores[new_size] = pm->scores[i];
      for (int eatenIdx = 0 ;; eatenIdx++)
      {
        if (pm->eaten[i][eatenIdx].x == 0 && pm->eaten[i][eatenIdx].y == 0)
          break;
        cpm.eaten[new_size][eatenIdx] = pm->eaten[i][eatenIdx];
      }
      new_size++;
    }
  }
  destroy_pm(*pm);
  pm->eaten = cpm.eaten;
  pm->from = cpm.from;
  pm->to = cpm.to;
  pm->scores = cpm.scores;
  pm->size = present;

  return new_size;
}

int ai_move(Field *field, int deepness)
{
  PossibleMoves pm = init_pm(512);
  int size = get_possible_moves(*field, &pm);
  int bestScore = 100;
  if (deepness % 2 == 1)
    bestScore = -bestScore;
  int bestI = 0;
  for (int i = 0; i < size; i++)
  {
    int score = 0;
    if (deepness != 1)
    {
      Field copied = copy(*field);
      move(&copied, pm.from[i], pm.to[i], pm.eaten[i]);
      score = pm.scores[i];
      if (deepness % 2 == 0)
        score = -score;
      score += ai_move(&copied, deepness - 1);
      destroy_field(copied);
    }
    else
    {
      score = pm.scores[i];
    }
    if ((deepness % 2 == 1 && score > bestScore)
     || (deepness % 2 == 0 && score < bestScore))
    {
      bestScore = score;
      bestI = i;
    }
  }
  if (size)
  {
    move(field, pm.from[bestI], pm.to[bestI], pm.eaten[bestI]);
  }
  destroy_pm(pm);
  return bestScore;
}

void game_loop(Field field, int deepness, int ai_enabled)
{
  while (1)
  {
    print_field(field);
    PossibleMoves pm = init_pm(512);
    int size = get_possible_moves(field, &pm);
    if (size == 0)
    {
      printf("Game over!");
      break;
    }
    for (int i = 0; i < size; i++)
    {
      printf("#%i\t(%i,%i) -> (%i,%i)\n", i, pm.from[i].x + 1, pm.from[i].y + 1, pm.to[i].x + 1, pm.to[i].y + 1);
    }
    printf("Make your choice: ");
    int moveIdx = 0;
    scanf("%i", &moveIdx);
    if (moveIdx < 0 || moveIdx >= size)
    {
      printf("Invalid move\n");
      continue;
    }
    else
    {
      move(&field, pm.from[moveIdx], pm.to[moveIdx], pm.eaten[moveIdx]);
      if (ai_enabled)
      {
        print_field(field);
        ai_move(&field, deepness * 2 - 1);
      }
    }
    destroy_pm(pm);
  }
}

int run_game()
{
  int deepness, height, width, ai_enabled = 1;
  printf("Glubina prosmotra hodov: ");
  scanf("%i", &deepness);
  //deepness = 5;
  printf("Shirina: ");
  scanf("%i", &width);
  //width = 8;
  printf("Dlina: ");
  scanf("%i", &height);
  //height = 8;
  if (width < 0)
  {
    printf("Invalid width!");
    return -1;
  }
  if (height < 7)
  {
    printf("Invalid height!");
    return -1;
  }
  printf("\n");
  printf(
    "Igra s chelovekom - 0\n" \
    "Igra s computerom - 1\n" \
    "Vvedite svoi vibor: ");
  scanf("%i", &ai_enabled);

  Field field = init_field(width, height);
  game_loop(field, deepness, ai_enabled);

  destroy_field(field);
  return 0;
}

int main(void)
{
  return run_game();
}
